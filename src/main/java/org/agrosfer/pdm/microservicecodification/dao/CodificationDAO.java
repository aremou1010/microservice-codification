/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicecodification.dao;

import java.util.Optional;
import org.agrosfer.pdm.microservicecodification.model.Reference;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface CodificationDAO extends JpaRepository<Reference, Long> {
    Optional<Reference> findByCode(String code);
}
