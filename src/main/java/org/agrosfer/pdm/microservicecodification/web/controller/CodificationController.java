/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicecodification.web.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microservicecodification.configs.ApplicationPropertiesConfig;
import org.agrosfer.pdm.microservicecodification.dao.CodificationDAO;
import org.agrosfer.pdm.microservicecodification.model.Reference;
import org.agrosfer.pdm.microservicecodification.web.exceptions.ReferenceCodeExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author g3a
 */
@RestController
public class CodificationController {
    
    @Autowired
    private CodificationDAO codificationDAO;
    
    @Autowired
    ApplicationPropertiesConfig apc;
    
    @PostMapping("/references")
    public ResponseEntity<?> createReference(@RequestBody Reference reference) throws URISyntaxException{
        
        
        //Build code
        String shortName = "";
        switch (reference.getEntity().toLowerCase()) {
            case "order":
                shortName = "ORD";
                break;

            case "sector":
                shortName = "SCT";
                break;

            case "product":
                shortName = "PDT";
                break;
                
            case "client":
                shortName = "CLT";
                break;

            case "user":
                shortName = "USR";
                break;
        }
        
        String currentTimestamp = String.valueOf(System.currentTimeMillis());
        char[] ak = currentTimestamp.toCharArray();
        
        Long somme = 0l;
        for (int i = 0; i < ak.length; i++) {
            char c = ak[i];
            //System.out.println(c + " ");
            somme+=Integer.parseInt(String.valueOf(c));
        }
        
        String code = shortName+"-"+somme;
        
        Optional<Reference> r = codificationDAO.findByCode(code);
        while (r.isPresent()) {
            somme++;
            code = shortName+"-"+somme;
            r = codificationDAO.findByCode(code);
        }
        
        
        Reference newReference = new Reference();
        newReference.setEntity(reference.getEntity());
        newReference.setCode(code);
        
        newReference = codificationDAO.save(newReference);
        
        if (newReference != null) {
        return ResponseEntity.ok()
                .location(new URI(apc.getUrl()+"/references/"+newReference.getId()))
                .body(newReference);
        }
        
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Reference cannot be created");
    }
    
    @GetMapping("/references")
    public ResponseEntity<?> allReferences (){
        return ResponseEntity.ok(codificationDAO.findAll());
        
    }
    
}
