/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicecodification.web.exceptions;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author g3a
 */
//@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ReferenceCodeExistException extends DataIntegrityViolationException {
    
    public ReferenceCodeExistException(String msg) {
        super(msg);
    }
    
}
