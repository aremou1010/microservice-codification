/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microservicecodification.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
@Entity
public class Reference implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String entity;
    
    @Column(unique = true)
    private String code;

    public Reference() {
    }

    public Reference(Long id, String entity, String code) {
        this.id = id;
        this.entity = entity;
        this.code = code;
    }    
    
    
}
